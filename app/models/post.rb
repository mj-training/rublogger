# frozen_string_literal: true

class Post < ApplicationRecord
  # Associations
  belongs_to :user
  has_many :like, dependent: :destroy
  has_many :comment, dependent: :destroy

  # Validations
  validates :title, :content, :hidden?, :user, presence: true
  validates :hidden?, exclusion: [nil]

  def initialize(attributes = {  })
    @title = attributes[:title]
    @content = attributes[:content]
    @hidden = false
    @user_id = attributes[:user_id]
  end
end
