# frozen_string_literal: true

class User < ApplicationRecord
  before_create :set_username
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # Associations
  has_many :post, dependent: :destroy
  has_many :comment, dependent: :destroy
  has_many :like, dependent: :destroy

  def admin?
    role == 'admin'
  end

  private

  def set_username
    self.username = email[/^[^@]+/]
  end

  # TODO: Add username using the form (sign_up form), devise stuff... override
  # TODO: Change role to enum like this: DATA_TYPES  = { text: 0, date: 1, drop_down: 2 }.freeze
end
