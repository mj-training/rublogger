# Requirements | Ideas | Initial Design

## Post

- A list of posts only shows the post title and a bit of the content
- After showing a post, it should contain:
  1. The post title
  2. Number of likes next to title
  3. Like button, or unlike button
  4. Comment text area and button
  5. comments down below
- Editing a post includes only editing the title or the content
- Deleting a post can only be done by the blogger

## Like

- To create a like, simply show a post then click the like button, if clicked then change button to unlike (use ajax for this?)

## Comment

- To create a comment, simply show a post then write the content inside a text area then click comment.

## Admin

- Can hide a post if clicking the hidden button on a post
- Can see all posts regardless of hidden state or date
- Can not edit or delete posts by bloggers
- Can see blogger profile: username, email, posts, comments, likes

## Blogger

- Can see only unhidden posts and date less than a week
- Can hide own post
- Can edit own post
- Can delete own post
- Can see only other bloggers profiles: username, posts

# Visual Design

- Start with ERB then convert to HAML syntax later
- Navbar
- Footer
- Pages:
  1. Home (Greeting (condition if logged in -> link to posts) else sign in or sign up )
  2. Sign in, Sign up -> forms
  3. Posts - Sort DESC by date, filter by own
  4. Post
  5. Profile

### Notes

- Use cancancan for authorization
- Include comments and likes inside the post view (.includes to avoid n+1)
- Rspec models (associations, validations, methods), requests (cruds and custom) & views later...
- In Models: Validations, Associations, permitted values, initalize if necessary
- check todos in files, make sure to change belongs_to -> references
