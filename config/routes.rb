# frozen_string_literal: true

Rails.application.routes.draw do
  # TODO: Stop routes to comments and likes
  # resources :likes
  # resources :comments
  resources :posts
  devise_for :users
  get 'home/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'
end
