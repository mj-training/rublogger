class ChangeColumnNameHiddenPosts < ActiveRecord::Migration[6.1]
  def change
    remove_column :posts, :hidden?
    add_column :posts, :hidden, :boolean, default: false, null: false
  end
end
