# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.string :title, default: '', null: false
      t.text :content, default: '', null: false
      t.boolean :hidden?, default: false, null: false
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
